function cacheFunction(cb) {
   let cache = {};
   return function (...value) {
      if (cache[value]) {
         return cache[value];
      }
      else {
         cache[value] = cb(...value);
         return cache[value];
      }
   }

}

module.exports = cacheFunction;

