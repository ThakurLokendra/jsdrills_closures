function limitFunctionCallCount(cb, n) {
    let count = 0;
    return function () {
        if (count < n) {
            cb();
            count += 1;
        } else {
            return null;
        }

    }
}

module.exports = limitFunctionCallCount;