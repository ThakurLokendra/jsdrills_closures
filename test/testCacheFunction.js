const cacheFunction = require('/home/lokendra/js4/cacheFunction.js');


function callBack(value1, value2) {
    return value1 / value2;
}


const result = cacheFunction(callBack);

console.log(result(2, 5));
console.log(result(10, 5));
console.log(result(10, 2));
console.log(result(2, 5));



